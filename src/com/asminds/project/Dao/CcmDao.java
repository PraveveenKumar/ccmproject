package com.asminds.project.Dao;

import java.util.List;

import com.asminds.project.CcmPojo.CcmPojo;
import com.asminds.project.userlogin.EmploginPojo;

public interface CcmDao {

	public boolean insert(CcmPojo s);
	public boolean delete(String s);
	public boolean update(CcmPojo a);
	public List<CcmPojo> validateUser(CcmPojo login);
	public List<CcmPojo> retrieveemp(CcmPojo login);
	public List<CcmPojo> retrieveempp(CcmPojo login);
}
