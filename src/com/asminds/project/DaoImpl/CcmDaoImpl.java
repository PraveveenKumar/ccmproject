package com.asminds.project.DaoImpl;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.asminds.project.CcmPojo.CcmPojo;
import com.asminds.project.Dao.CcmDao;
import com.asminds.project.userlogin.EmploginPojo;

public class CcmDaoImpl implements CcmDao {

	public boolean check = false;

	public boolean insert(CcmPojo s) {

		List<String> li = this.checkEmpId();

		Iterator<String> it = li.iterator();

		while (it.hasNext()) {

			if (it.next().equals(s.getEmpid())) {
				check = true;
			}

		}

		if (check == false) {
			Configuration cfg = new Configuration();
			cfg.configure("hibernate.cfg.xml");
			SessionFactory sf = cfg.buildSessionFactory();
			Session ss = sf.openSession();
			Transaction tx = ss.beginTransaction();
			ss.save(s);
			System.out.println("I am Saving");
			tx.commit();
			ss.close();
			sf.close();
			return true;
		}

		else {
			System.out.println("Employee Id is Already Exists");
			return false;
		}

	}

	public List<String> checkEmpId() {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory sf = cfg.buildSessionFactory();
		Session ss = sf.openSession();
		Transaction tx = ss.beginTransaction();
		@SuppressWarnings("deprecation")
		Query q = ss.createSQLQuery("select Empid from CCMPojo o");
		@SuppressWarnings("unchecked")
		List<String> l = q.list();
		System.out.println("Total numbers=" + l.size());
		System.out.println(l.toString());
		return l;
	}

	public List<CcmPojo> validateUser(CcmPojo login) {
		System.out.println(login.getEmpid() + " " + login.getPassword());
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		Query qry = session.createQuery("from CcmPojo where empid=:empid and password=:password");
		qry.setParameter("empid", login.getEmpid());
		qry.setParameter("password", login.getPassword());
		List<CcmPojo> l = qry.list();
		System.out.println("Total Number Of Records : " + l.size());
		Iterator it = l.iterator();

		while (it.hasNext()) {
			Object o = (Object) it.next();
			CcmPojo p = (CcmPojo) o;
			System.out.println("userName : " + p.getEmpid());
			System.out.println("Password : " + p.getPassword());
			System.out.println("----------------------");
		}
		session.close();
		factory.close();
		return l;
	}

	public List<CcmPojo> retrieveemp(CcmPojo login) {

		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		Query qry = session.createQuery("from CcmPojo where designation=:designation");
		qry.setParameter("designation", login.getDesignation());

		List<CcmPojo> l = qry.list();
		System.out.println("Total Number Of Records : " + l.size());
		Iterator it = l.iterator();

		while (it.hasNext()) {
			Object o = (Object) it.next();
			CcmPojo p = (CcmPojo) o;
			System.out.println("Designation : " + p.getDesignation());
		}
		session.close();
		factory.close();
		return l;

	}

	public List<CcmPojo> retrieveempp(CcmPojo login) {

		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		Query qry = session.createQuery("from CcmPojo where place=:place");
		qry.setParameter("place", login.getPlace());

		List<CcmPojo> l = qry.list();
		System.out.println("Total Number Of Records : " + l.size());
		Iterator it = l.iterator();

		while (it.hasNext()) {
			Object o = (Object) it.next();
			CcmPojo p = (CcmPojo) o;
			System.out.println("Place : " + p.getPlace());
		}
		session.close();
		factory.close();
		return l;

	}

	public boolean delete(String string) {
		// TODO Auto-generated method stub
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory sf = cfg.buildSessionFactory();
		Session ss = sf.openSession();
		Transaction tx = ss.beginTransaction();
		Object object = ss.load(CcmPojo.class, new Integer(string));
		CcmPojo po = (CcmPojo) object;
		ss.delete(po);
		tx.commit();
		ss.close();
		sf.close();
		return true;

	}

	public boolean update(CcmPojo a) {

		String i = a.getEmpid();

		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");

		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		Object o = session.load(CcmPojo.class, new Integer(i));
		CcmPojo s = (CcmPojo) o;

		Transaction tx = session.beginTransaction();

		s.setEmpname(a.getEmpname());
		s.setDesignation(a.getDesignation());
		s.setDoj(a.getDoj());
		s.setDob(a.getDob());
		s.setEmailid(a.getEmailid());
		s.setPlace(a.getEmailid());
		s.setPassword(a.getPassword());

		tx.commit();

		System.out.println("Object Updated successfully.....!!");
		session.close();
		factory.close();
		return true;

	}

}
