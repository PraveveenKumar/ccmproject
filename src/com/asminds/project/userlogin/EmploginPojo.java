package com.asminds.project.userlogin;

public class EmploginPojo {
	String username;
	String password;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public EmploginPojo(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	public EmploginPojo() {
		super();
	}
	@Override
	public String toString() {
		return "UserloginPojo [username=" + username + ", password=" + password + "]";
	}
	
}
