package com.asminds.project.CcmController;

import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.asminds.project.CcmPojo.CcmPojo;
import com.asminds.project.DaoImpl.CcmDaoImpl;
import com.asminds.project.login.LoginPojo;
import com.asminds.project.userlogin.EmploginPojo;

@Controller
public class CcmController {

	@RequestMapping("/")
	public String index() {
		System.out.println("I am in index page");
		return "index";
	}

	@RequestMapping("/Admin")
	public String admin() {
		System.out.println("I am in Admin method");
		return "adminlogin";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView formvalidation(@ModelAttribute("user") LoginPojo user) {
		String username = user.getUsername();
		String password = user.getPassword();
		String wrong = "Sorry! Invalid Username or Password...";
		if (username.equals("admin") && password.equals("admin")) {
			return new ModelAndView("adminview");
		} else {
			return new ModelAndView("index", "wrong", wrong);
		}

	}

	@RequestMapping("/User")
	public String user() {
		System.out.println("I am in User Method ");
		return "emplogin";
	}

	@RequestMapping(value = "/emplogin", method = RequestMethod.POST)
	public ModelAndView emplogin(@ModelAttribute("s") CcmPojo a) {
		String wrong = "Sorry! Invalid User Name & Password";
		CcmDaoImpl i = new CcmDaoImpl();
		System.out.println("I am in validation page");
		List<CcmPojo> v = i.validateUser(a);
		System.out.println("I am in Validating a Password");
		Iterator it = v.iterator();

		if (v.size() != 0) {
			return new ModelAndView("empview");
		} else {
			return new ModelAndView("emplogin", wrong, "Wrong Password or Username !!! ");
		}
	}

	@RequestMapping(value = "/retrieve", method = RequestMethod.POST)
	public ModelAndView empretrieve(@ModelAttribute("s") CcmPojo a) {
		String wrong = "Sorry! Invalid Designation";
		CcmDaoImpl i = new CcmDaoImpl();
		System.out.println("I am in retrieve page");
		List<CcmPojo> v = i.retrieveemp(a);
		System.out.println("I am in Validating a designation");
		Iterator it = v.iterator();

		if (v.size() != 0) {
			return new ModelAndView("adminview", "v", v);
		} else {
			return new ModelAndView("adminview", wrong, "Wrong Place!!! ");
		}
	}

	@RequestMapping(value = "/retrievep", method = RequestMethod.POST)
	public ModelAndView empretrievep(@ModelAttribute("s") CcmPojo a) {
		String wrong = "Sorry! Invalid Place";
		CcmDaoImpl i = new CcmDaoImpl();
		System.out.println("I am in retrieve page");
		List<CcmPojo> v = i.retrieveempp(a);
		System.out.println("I am in Validating a Place");
		Iterator it = v.iterator();

		if (v.size() != 0) {
			return new ModelAndView("adminview", "v", v);
		} else {
			return new ModelAndView("adminview", "wrong", "Wrong Place!!! ");
		}
	}

	@RequestMapping("/reg")
	public String reg() {
		System.out.println("I am in Registration method");
		return "reg";
	}

	@RequestMapping(value = "/Insert", method = RequestMethod.POST)
	public String insert(CcmPojo s) {
		CcmDaoImpl l = new CcmDaoImpl();
		boolean n = l.insert(s);
		System.out.println(n);
		if (n == true) {
			System.out.println("I am in Insert the Datas into Database");
			return "emplogin";
		} else {
			return "reg";
		}
	}

	@RequestMapping("/update")
	public String update(@ModelAttribute("t") CcmPojo ad) {

		System.out.println(" Im in update method");

		CcmDaoImpl a = new CcmDaoImpl();
		boolean b = a.update(ad);
		if (b == true) {

			return "reg";
		} else {
			return "index";
		}

	}

	/*
	 * @RequestMapping("/viewall") public ModelAndView view() {
	 * System.out.println("View all method"); CcmDaoImpl s = new CcmDaoImpl();
	 * List<CcmPojo> l = s.viewall();
	 * 
	 * Iterator<CcmPojo> it = l.iterator();
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * while (it.hasNext()) {
	 * 
	 * Object o = (Object) it.next(); CcmPojo p = (CcmPojo) o;
	 * System.out.println("Employee Details : " + p.getEmpid() + " " +
	 * p.getEmpname() + " " + p.getDesignation() + " " + p.getDoj() + " " +
	 * p.getDob() + " " + p.getEmailid() + " " + p.getPlace() + " " +
	 * p.getPassword()); }
	 * 
	 * return new ModelAndView("viewpage", "praveen", l); }
	 * 
	 * @RequestMapping("/back") public String back() {
	 * System.out.println("I am in Back method"); return "emplogin"; }
	 */
}
