package com.asminds.project.CcmPojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CcmPojo {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;
	String empid;
	String empname;
	String designation;
	String doj;
	String dob;
	String emailid;
	String place;
	String password;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmpid() {
		return empid;
	}

	public void setEmpid(String empid) {
		this.empid = empid;
	}

	public String getEmpname() {
		return empname;
	}

	public void setEmpname(String empname) {
		this.empname = empname;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDoj() {
		return doj;
	}

	public void setDoj(String doj) {
		this.doj = doj;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public CcmPojo(int id, String empid, String empname, String designation, String doj, String dob, String emailid,
			String place, String password) {
		super();
		this.id = id;
		this.empid = empid;
		this.empname = empname;
		this.designation = designation;
		this.doj = doj;
		this.dob = dob;
		this.emailid = emailid;
		this.place = place;
		this.password = password;
	}

	public CcmPojo() {
		super();
	}

	@Override
	public String toString() {
		return "CcmPojo [id=" + id + ", empid=" + empid + ", empname=" + empname + ", designation=" + designation
				+ ", doj=" + doj + ", dob=" + dob + ", emailid=" + emailid + ", place=" + place + ", password="
				+ password + "]";
	}

}
