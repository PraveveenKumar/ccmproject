<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration</title>
</head>
<bodystyle="background-color:#F08080">
<center>
	<td>Employee Details</td> <br> <br>
	<form action="Insert">
		Employee Id: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		<input type="text" name="empid"><br> <br>
		Employee Name:&nbsp&nbsp&nbsp&nbsp
		<input type="text" name="empname"><br><br> 
		Employee Designation:&nbsp
		<input type="text"	name="designation"> <br> <br> 
		Employee doj:&nbsp<input type="date" name="doj"> <br><br> 
		Employee dob:&nbsp<input type="date" name="dob"> <br> <br>
		Employee Email Id:&nbsp<input type="text" name="emailid"><br><br>
		Employee Place :&nbsp<input type="text" name="place"><br><br>
		Employee Password :&nbsp<input type="password" name="password"><br><br>
		<input type="submit" value="Register"><br> <br>
</form>
</center>
</body>
</html>
 --%>

<!DOCTYPE html>
<html>
<head>
<script>
function validateForm() {
  var x = document.forms["myForm"]["empid"].value;
  if (x == "") {
    alert("All Fields must be filled out");
    return false;
  }
}
</script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  background-color: black;
}

* {
  box-sizing: border-box;
}

/* Add padding to containers */
.container {
  padding: 16px;
  background-color: white;
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for the submit button */
.registerbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity: 1;
}

/* Add a blue text color to links */
a {
  color: dodgerblue;
}

/* Set a grey background color and center the text of the "sign in" section */
.signin {
  background-color: #f1f1f1;
  text-align: center;
}
</style>
</head>
<body>

<form name="myForm" action="Insert" onsubmit="return validateForm()" method="post" >
  <div class="container">
    <h1>Register</h1>
    <p>Please fill in this form to create an account.</p>
    <hr>

    <label for="empid"><b>Employee Id</b></label>
    <input type="text" placeholder="Enter Employee Id" name="empid" required>

    <label for="empname"><b>Employee Name</b></label>
    <input type="text" placeholder="Enter Employee Name" name="empname" required>

    <label for="designation"><b>Designation</b></label>
    <input type="text" placeholder="Enter Designation" name="designation" required>
  
   <label for="doj"><b>Employee Date of Joining</b></label>
    <input type="date" placeholder="Enter Employee doj" name="doj" required>

    <label for="dob"><b>Employee Date of Birth</b></label>
    <input type="date" placeholder="Enter Employee dob" name="dob" required><br><br>

    <label for="emailid"><b>Email Id</b></label>
    <input type="text" placeholder="Enter Email Id" name="emailid" required>
  
  <label for="place"><b>Place</b></label>
    <input type="text" placeholder="Enter Employee Place" name="place" required>

    <label for="password"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" required>
 
  
    <hr>
    <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>

    <button type="submit" class="registerbtn">Register</button>
  </div>
  
  
</form>

</body>
</html>
