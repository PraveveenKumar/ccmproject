<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Select Roles</title>
</head>
<body bgcolor="#dsfsdg">
	<h1>Roles:</h1>
	<form action="retrieve" method="post">
		<select name="designation">
		<option value="Select any one" selected>Select any one
				</option>
		<option value="Senior Designer" >Senior Designer
				</option>
			<option value="HR">HR</option>
			<option value="JuniorEngineer">JuniorEngineer</option>
			<option value="Designer">Designer</option>

		</select>
		<input type="submit" value="Submit">
	</form>
	
	<form action="retrievep" method="post">
	<P align=right>
		<select name="place">
		<option value="Select any one" selected>Select any one
				</option>
		<option value="Chennai" >Chennai
				</option>
			<option value="Madurai">Madurai</option>
			<option value="Virudhunagar">Virudhunagar</option>
			<option value="Coimbatur">Coimbatur</option>

		</select>
		<input type="submit" value="Submit">
	</P>
	</form>
	<table border="1" style="width: 100%">
				<tr>
					<th>Employee ID</th>
					<th>Employee Name</th>
					<th>Designation</th>
					<th>Date Of Joining</th>
					<th>Date Of Birth</th>
					<th>Email Id</th>
					<th>Place</th>
					<th>Password</th>
					<th>Delete</th>
					<th>Update</th>
				</tr>
				<c:if test="${not empty v}">
		<c:forEach var="n" items="${v}">
				<tr>
					<td><c:out value="${n.empid}" /></td>
					<td><c:out value="${n.empname}" /></td>
					<td><c:out value="${n.designation}" /></td>
					<td><c:out value="${n.doj}" /></td>
					<td><c:out value="${n.dob}" /></td>
					<td><c:out value="${n.emailid}" /></td>
					<td><c:out value="${n.place}" /></td>
					<td><c:out value="${n.password}" /></td>
					<td><form action="delete"><input type="submit" value="Delete"></form></td>
					<td><form action="update"><input type="submit" value="Update"></form></td>
				</tr>
</c:forEach>
	</c:if>
			</table>
</body>
</html>